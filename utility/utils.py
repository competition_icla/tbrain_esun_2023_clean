import pandas as pd
import numpy as np
import json


def outputSubmissionFile(prediction, modelName, now):
    example = pd.read_csv("../train_data/submission_example.csv")
    # Public data until 0-600181
    example.loc[: len(prediction) - 1, "pred"] = np.where(prediction < 0.5, 0, 1)

    with open(
        f"../prediction/{modelName}_{now}.csv",
        mode="w",
        newline="\n",
    ) as f:
        example.to_csv(f, sep=",", index=False)


def outputParameters(params: dict, f1Score: list[float], modelName: str, now):
    # Hyperparameters + mean f1 score from validation.
    f1ScoreStr = [str(f1Score[i]) for i in range(len(f1Score))]
    with open(f"../prediction/{modelName}_params_{now}.txt", "w") as f:
        f.write(json.dumps(params))
        f.write(f"\n\nmean MAPE from validation for this run: {np.mean(f1Score)}\n")
        f.write("\nMAPE of all runs:")
        f.write("\n".join(f1ScoreStr))


def outputFeatures(columns: list, modelName: str, now):
    # Features.
    np.savetxt(
        f"../prediction/{modelName}_features_{now}.txt",
        columns,
        fmt="%s",
    )


def outputFeatureImportance(featImportance: np.ndarray, modelName: str, now):
    # Features.
    np.savetxt(
        f"../prediction/{modelName}_feature_importance_{now}.txt",
        featImportance,
        fmt="%s",
    )
