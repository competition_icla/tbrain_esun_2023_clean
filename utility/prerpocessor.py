import numba as nb
import pandas as pd
from utility.constants import catCols, intCols, floatCols


def setDataType(data: pd.DataFrame) -> pd.DataFrame:
    """
    The data will be read in str type.
    Set each column to the corresponding data type.
    """
    for col in catCols.keys():
        data[col] = data[col].astype("category")
    for col in intCols.keys():
        data[col] = data[col].astype(float)
        data[col] = data[col].astype(int)
    for col in floatCols.keys():
        data[col] = data[col].astype(float)

    return data


def addMostFrequentValue(x: pd.DataFrame, col: str, name: str) -> pd.DataFrame:
    """
    Add a new feature: mostFreq<COL>.
    Ex: if Taiwan is the most frequent country of the purchase, then
        x["isMostFreqCountry"] = 1 or 0 based on the value at each row in
        x["stocn"].

    Parameters:
        x: pd.DataFrame (group) of data for one user
        col: which column to base on
        name: feature name (not necessarily column name)

    Return:
        x: pd.DataFrame
    """

    temp = x[col].copy()
    try:
        temp["MostFreq" + name] = x[col].mode()[0]
        x["isMostFreq" + name] = x[col] == temp["MostFreq" + name]
    except KeyError:
        x["isMostFreq" + name] = False

    return x


def addMostFrequentValueIteration(
    x: pd.DataFrame,
    mostFreqCols: list[str],
    mostFreqNames: list[str],
) -> pd.DataFrame:
    """
    Iterate over columns of interest to see if each row has the most frequent
    value among the data.
    """

    for col, name in zip(mostFreqCols, mostFreqNames):
        x = addMostFrequentValue(x, col, name)

    return x


@nb.njit(fastmath=True)
def addMostFrequentValueNumba(x, modeVal):
    """
    Parameters:
        x: nb.typed.List[str]; the target column, eg. isMostFreqCountry, which is initialized
            to stncn (TransactionCountry).
        modeVal: str; the most frequent value in the original column.

    Return:
        x: nb.typed.List[bool];
    """

    n = len(x)
    for i in range(n):
        x[i] = "1" if x[i] == modeVal else "0"

    return x


def extractHour(x: pd.Series) -> pd.Series:
    if len(x["loctm"]) == 6:
        x["hour"] = x["loctm"][:2]
    elif len(x["loctm"]) == 5:
        x["hour"] = x["loctm"][:1]
    else:
        x["hour"] = "-1"

    return x


@nb.njit(fastmath=True)
def extractHourArray(loctm, hour):
    n = len(loctm)
    for i in range(n):
        d = str(loctm[i])
        if len(d) == 6:
            hour[i] = d[:2]
        elif len(d) == 5:
            hour[i] = d[:1]

    return hour


def preprocess(
    filePath: str,
    colToGroupby: str = "cano",
    ifAddMostFreq: bool = False,
    mostFreqCols: list[str] = catCols.keys(),
    mostFreqNames: list[str] = catCols.values(),
    ifExtractHour: bool = False,
    toSave: bool = False,
) -> pd.DataFrame:
    """
    Pipeline of preprocessing. The results is saved to the original file.
    """

    # Set data type.
    data = pd.read_csv(filePath, dtype="str")
    data["label"] = data["label"].astype(int)

    # Add new features for each customer.
    if ifAddMostFreq is True:
        # Initialize isMostFreq<NAME> to col.
        for col, name in zip(mostFreqCols, mostFreqNames):
            data["isMostFreq" + name] = data[col]

        groups = data.groupby(colToGroupby)
        for name, group in groups:
            # Process group by group.
            for name in mostFreqNames:
                modeValue = group[col].mode()[0]
                colname = "isMostFreq" + name
                # Store values to the original data.
                data.loc[group.index, colname] = addMostFrequentValueNumba(
                    x=nb.typed.List(group[colname].values),
                    modeVal=modeValue,
                )

    # Extract hour from loctm.
    if ifExtractHour is True:
        # Make sure loctm is of type str.
        data["loctm"] = data["loctm"].astype("str")

        # Initialize hour to be "-1".
        data["hour"] = "-1"

        # Divide and conquer.
        # Set cursors and chunk size.
        chunkSize = 10000
        iStart = 0
        iEnd = (iStart + 1) * chunkSize
        # Set a counter to avoid dead lock.
        counter = 0
        while iEnd <= len(data) and counter < int(len(data) / chunkSize) + 1:
            # Process one chunk at a time and store the value in the original
            # dataframe.
            smallDF = data[iStart:iEnd][["loctm", "hour"]]
            hour = extractHourArray(
                nb.typed.List(smallDF["loctm"].values),
                nb.typed.List(smallDF["hour"].values),
            )
            data.loc[smallDF.index, "hour"] = hour

            # Move cursor.
            iStart += 1
            iEnd = (iStart + 1) * chunkSize
            if counter % 100 == 0:
                print(f"processing {iStart} to {iEnd}")
            counter += 1

            # Handle the last chunk.
            if iEnd > len(data):
                iEnd = len(data)

    # Save data to file.
    if toSave is True:
        data.to_csv(filePath, index=False)

    return data
