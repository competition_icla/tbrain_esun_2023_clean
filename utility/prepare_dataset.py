import pandas as pd


def loadFeatures(
    features: list[str],
    prefix: str,
    dataType: str,
    dir: str = "../train_data/",
):
    data = pd.DataFrame()
    for feat in features:
        filePath = f"{dir}{dataType}_{prefix}{feat}.csv"
        df = pd.read_csv(filePath)
        data = pd.concat([data, df], axis=1)

    return data


def loadAllData(
    dataType: str,
    useCols: list[str],
    ifTimeData: bool = False,
    derivedCols: list[str] = [],
):
    if dataType == "training":
        dataList = [dataType]
    elif dataType == "public" or dataType == "public_reveal":
        dataList = ["training", dataType]
    elif dataType == "private":
        # The released public data has more records than the previously
        # released one. Use it for feature engineering and reference for private.
        dataList = ["training", "public_reveal", dataType]
    else:
        print(f"Unsupported data type {dataType}")
        return pd.DataFrame()

    data = pd.DataFrame()
    labelExist = 0
    for d in dataList:
        # Remove "label" from useCols if it is test data.
        if "label" in useCols and d == dataList[-1] and d != "training":
            useCols = set(useCols)
            useCols.remove("label")
            useCols = list(useCols)
            labelExist = 1

        cData = loadCertainData(
            d,
            useCols=useCols,
            ifTimeData=ifTimeData,
            derivedCols=derivedCols,
        )
        cData["group"] = d
        cData["subIndex"] = cData.index

        # Set label of test data to zero, so it doesn't affect feature engineering
        # regarding fraud count. In addition, it is to make sure the columns
        # are consistent.
        if labelExist == 1 and d == dataList[-1] and d != "training":
            cData["label"] = 0

        data = pd.concat([data, cData], axis=0, ignore_index=True)

    return data


def loadCertainData(
    dataType: str,
    useCols: list[str],
    ifTimeData: bool,
    derivedCols: list[str],
):
    mainFile = f"../train_data/{dataType}.csv"
    data = pd.read_csv(mainFile, usecols=useCols)

    if ifTimeData is True:
        timeFile = f"../train_data/{dataType}_time.csv"
        timeData = pd.read_csv(timeFile)

        data = pd.concat([data, timeData], axis=1)

    if len(derivedCols) > 0:
        for col in derivedCols:
            extData = pd.read_csv(f"../train_data/{dataType}_{col}.csv")
            data = pd.concat([data, extData], axis=1)

    return data
