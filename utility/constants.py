intCols = {
    "locdt": "TransactionDate",
    "loctm": "TransactionTime",
    "iterm": "InstallmentTerms",
    "contp": "TransactionType",
    "etymd": "TransactionMethod",
    "mchno_ord": "MachineNumber",
    "acqic_ord": "AcquiringBankCode",
    "mcc": "MccCode",
    "mcc_ord": "MccCode",
    "mcc_hour_ord": "MccCodeHour",
    "stocn_hour_ord": "CountryHour",
    "scity_hour_ord": "CityHour",
    "insfg": "IsInstallmentTransaction",
    "bnsfg": "IsBonusDeal",
    "stocn": "TransactionCountry",
    "scity": "TransactionCity",
    "stscd": "StatusCode",
    "ovrlt": "OverdraftCode",
    "flbmk": "FallbackMark",
    "hcefg": "PaymentType",
    "csmcu": "TransactionCurrency",
    "flg_3dsmk": "3DTransactionMark",
    "ecfg": "OnlineTransactionMark",
    "hour": "Hour",
}

floatCols = {
    "conam": "TransactionAmountNTD",
    "csmam": "TransactionAmount",
    "flam1": "ActualPaymentAmount",
}

catCols = {
    "chid": "CustomerID",
    "cano": "CardNumber",
    "mchno": "MachineNumber",
    "acqic": "AcquiringBankCode",
    "stocn_hour": "CountryHour",
    "scity_hour": "CityHour",
    "mcc_hour": "MccCodeHour",
}
