from lightgbm import LGBMRegressor
from sklearn.model_selection import StratifiedKFold, StratifiedGroupKFold
from sklearn.metrics import f1_score
from typing import Tuple
import numpy as np
import pandas as pd
import lightgbm as lgb
import optuna


def lgbF1Score(yPred, data) -> Tuple[str, float]:
    yTrue = data.get_label()
    yPred = np.where(yPred < 0.5, 0, 1)  # Convert probability to binary.

    # eval_name, eval_result, is_higher_better
    return "f1_score", f1_score(y_true=yTrue, y_pred=yPred), True


class MyLGBClassifier:
    def __init__(self) -> None:
        pass

    def setInputData(
        self,
        X: pd.DataFrame,
        y: pd.DataFrame,
        catCols: list[str] = [],
        groups: list[int] = [],
    ) -> None:
        self.X = X
        self.y = y
        self.cateCols = catCols
        self.groups = groups

    def setConfiguration(
        self,
        params=None,
        nFold: int = 5,
        nRepeats: int = 3,
        randomState: int = 17,
    ) -> None:
        if params is not None:
            self.lgbParams = params
        else:
            seed = randomState
            params = {
                "num_boost_round": 1000,
                "early_stopping_round": 50,
                "objective": "binary",
                "boosting": "gbdt",
                "metric": "custom",
                "learning_rate": 0.01,
                "lambda_l1": 0.01,
                "lambda_l2": 0.01,
                "bagging_freq": 7,
                "feature_fraction": 0.5,
                "bagging_fraction": 0.5,
                "min_gain_to_split": 0,
                "min_data_in_leaf": 7,
                "num_leaves": 16,
                "max_depth": 10,
                "seed": seed,
                "feature_fraction_seed": seed,
                "bagging_seed": seed,
                "drop_seed": seed,
                "data_random_seed": seed,
                "verbosity": -1,
                "n_jobs": -1,
            }
            self.lgbParams = params

        try:
            self.numBoostRound = self.lgbParams.pop("num_boost_round")
        except KeyError:
            self.numBoostRound = 1000
        try:
            self.earlyStoppingRound = self.lgbParams.pop("early_stopping_round")
        except KeyError:
            self.earlyStoppingRound = int(self.numBoostRound * 0.01)

        if len(self.groups) == len(self.y):
            self.kfoldProcessor = StratifiedGroupKFold(
                n_splits=nFold,
                shuffle=True,
                random_state=randomState,
            )
        else:
            self.kfoldProcessor = StratifiedKFold(
                n_splits=nFold,
                shuffle=True,
                random_state=randomState,
            )

    def runTraining(self):
        self.results = {
            "F1Score": [],
        }

        if len(self.groups) == len(self.y):
            splits = self.kfoldProcessor.split(self.X, self.y, self.groups)
        else:
            splits = self.kfoldProcessor.split(self.X, self.y)

        for i, (trainIdx, testIdx) in enumerate(splits):
            trainX, trainY, testX, testY = (
                self.X.iloc[trainIdx],
                self.y.iloc[trainIdx],
                self.X.iloc[testIdx],
                self.y.iloc[testIdx],
            )

            trainDS = lgb.Dataset(
                trainX,
                trainY,
                feature_name=list(trainX.columns),
                categorical_feature=self.cateCols,
            )
            valDS = lgb.Dataset(
                testX,
                testY,
                feature_name=list(trainX.columns),
                categorical_feature=self.cateCols,
            )

            # Train model.
            callbacks = [
                lgb.early_stopping(
                    stopping_rounds=self.earlyStoppingRound,
                    first_metric_only=True,
                ),
                lgb.callback.log_evaluation(period=100),
            ]
            self.model = lgb.train(
                params=self.lgbParams,
                train_set=trainDS,
                valid_sets=[trainDS, valDS],
                num_boost_round=self.numBoostRound,
                feval=lgbF1Score,
                callbacks=callbacks,
            )

            # Validate with test data.
            pred = self.model.predict(testX)

            # Record f1-score of test.
            yPred = np.where(pred < 0.5, 0, 1)
            currF1 = f1_score(y_true=testY.values, y_pred=yPred)
            self.results["F1Score"].append(currF1)

        print(np.mean(self.results["F1Score"]))

    def runTrainingOversample(self, toTrain, featCols):
        self.results = {
            "F1Score": [],
        }

        """
        Among 618898 unique card numbers in training.csv, there are only
        11501 cards with a fraud history (from day 0-55). I.e., there are
        607397 card numbers without any fraud record.

        The idea is to split these 607397 cards into 5 groups and then train
        the model on one of the groups + cards with fraud history at a time.

        The train/test split is still performed by StratifiedGroupKFold.
        """

        nSplitCano = 5

        neverFraudCano = np.loadtxt(
            "../train_data/training_neverFraudCano.txt", dtype=str, ndmin=1
        )
        np.random.shuffle(neverFraudCano)
        groupNFC = np.array_split(neverFraudCano, nSplitCano)

        withFraudHistoryCano = np.loadtxt(
            "../train_data/training_withFraudHistoryCano.txt", dtype=str, ndmin=1
        )
        withFraudHistoryData = toTrain[toTrain["cano"].isin(withFraudHistoryCano)]
        print(f"{len(withFraudHistoryData)} transactions from cards with prior frauds.")

        for j in range(nSplitCano):
            # Get subset of data
            neverFraudData = toTrain[toTrain["cano"].isin(groupNFC[j])]
            subTrain = pd.concat([neverFraudData, withFraudHistoryData], axis=0)
            subTrain.reset_index(drop=True, inplace=True)
            X = subTrain[featCols]
            y = subTrain["label"]
            print(f"{len(X)} transactions used in training split {j+1}")

            if len(self.groups) == len(self.y):
                groups = subTrain["locdt"] % 10
                splits = self.kfoldProcessor.split(X, y, groups)
                print("Using StratifiedGroupKFold.")
            else:
                splits = self.kfoldProcessor.split(X, y)

            for i, (trainIdx, testIdx) in enumerate(splits):
                print(f"Training # {j*nSplitCano + i + 1}")
                trainX, trainY, testX, testY = (
                    X.iloc[trainIdx],
                    y.iloc[trainIdx],
                    X.iloc[testIdx],
                    y.iloc[testIdx],
                )

                trainDS = lgb.Dataset(
                    trainX,
                    trainY,
                    feature_name=list(trainX.columns),
                    categorical_feature=self.cateCols,
                )
                valDS = lgb.Dataset(
                    testX,
                    testY,
                    feature_name=list(trainX.columns),
                    categorical_feature=self.cateCols,
                )

                # Train model.
                callbacks = [
                    lgb.early_stopping(
                        stopping_rounds=self.earlyStoppingRound,
                        first_metric_only=True,
                    ),
                    lgb.callback.log_evaluation(period=100),
                ]
                self.model = lgb.train(
                    params=self.lgbParams,
                    train_set=trainDS,
                    valid_sets=[trainDS, valDS],
                    num_boost_round=self.numBoostRound,
                    feval=lgbF1Score,
                    callbacks=callbacks,
                )

                # Validate with test data.
                pred = self.model.predict(testX)

                # Record f1-score of test.
                yPred = np.where(pred < 0.5, 0, 1)
                currF1 = f1_score(y_true=testY.values, y_pred=yPred)
                self.results["F1Score"].append(currF1)

        print(np.mean(self.results["F1Score"]))

    def predict(self, X):
        return self.model.predict(X)

    def paramScan(self, nTrials: int = 20):
        def objective(trial):
            params = {
                "learning_rate": trial.suggets_flost(
                    "learning_rate", 1.0e-4, 1.0e-2, log=True
                ),
                "lambda_l1": trial.suggets_flost("lambda_l1", 1.0e-8, 1.0e-6, log=True),
                "lambda_l2": trial.suggets_flost("labmda_l2", 1.0e-3, 10, log=True),
                "bagging_freq": trial.suggest_int("bagging_fre", 0, 7),
                "feature_fraction": trial.suggets_flost("feature_fraction", 0.1, 1.0),
                "bagging_fraction": trial.suggets_flost("bagging_fraction", 0.1, 1.0),
                "min_gain_to_split": trial.suggest_int("min_gain_to_split", 0, 1),
                "min_data_in_leaf": trial.suggest_int("min_data_in_leaf", 1, 10),
                "num_leaves": trial.suggest_int("num_leaves", 4, 16),
                "max_depth": trial.suggest_int("max_depth", 3, 10),
            }

            thisF1 = []
            for i, (trainIdx, testIdx) in enumerate(
                self.kfoldProcessor.split(self.X, self.y)
            ):
                trainX, trainY, testX, testY = (
                    self.X.iloc[trainIdx],
                    self.y.iloc[trainIdx],
                    self.X.iloc[testIdx],
                    self.y.iloc[testIdx],
                )
                trainDS = lgb.Dataset(trainX, trainY, feature_name=trainX.columns)
                valDS = lgb.Dataset(testX, testY, feature_name=trainX.columns)

                # Train model.
                callbacks = [
                    lgb.early_stopping(stopping_rounds=50, first_metric_only=True)
                ]
                model = lgb.train(
                    params=self.lgbParams,
                    train_set=trainDS,
                    valid_sets=[trainDS, valDS],
                    num_boost_round=1000,
                    feval=lgbF1Score,
                    callbacks=callbacks,
                )

                # Validate with test data.
                pred = model.predict(testX, num_iteration=model.best_iteration)

                # Record f1-scre of test.
                yPred = np.where(pred < 0.5, 0, 1)
                currF1 = f1_score(y_true=testY.values, y_pred=yPred)
                thisF1.append(currF1)

            print(params)
            print(f"mean F1Score of this trial: {np.mean(thisF1)}")

            return np.mean(thisF1)

        scan = optuna.create_study(direction="maximize")
        scan.enqueue_trial(self.lgbParams)
        scan.optimize(objective, n_trials=nTrials)

        bestTrial = scan.best_trial
        print(f"F1Score of best trial: {bestTrial.value}")
        print("Params of best trail:")
        for key, val in bestTrial.params.items():
            print(f'"{key}":{val},')
            # Update params from best trial.
            self.lgbParams[key] = val
