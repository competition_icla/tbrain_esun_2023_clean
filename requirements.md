# data analysis
pandas==2.1.2
numpy==1.26.1
openpyxl==3.1.2
matplotlib==3.8.1

# model
lightgbm==4.1.0
scikit-learn==1.3.2
optuna==3.4.0
numba==0.58.1

# notebook
ipykernel==6.26.0