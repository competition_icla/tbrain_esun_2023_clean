from setuptools import setup, find_packages

setup(
    name="utility",
    version="0.1.0",
    description="Utility function.",
    packages=find_packages(),
)
